# Changelog
Aquí se documentarán los cambios más importantes del proyecto

## [5.1] - 2021-12-08
## Changed
- Cambio de puerto para las comunicaciones entre máquinas en una LAN específica

## [5.0] - 2021-12-07
## Added 
- Archivo Socket
## Changed
- Tubería FIFO solo para el Socket del servidor

## [4.1] - 2021-12-07
## Added
- Tratamiento de señales **SIGINT**, **SIGTERM**, **SIGPIPE** y **SIGUSR2** en el servidor.


## [4.0] - 2021-12-06
## Added
- Programa del cliente y del servidor con sus respectivas clases MundoCliente MundoServidor
- Hilo lector de una FIFO en el servidor
- FIFO de cliente a servidor y una FIFO de servidor a cliente.
- Clase DatosCS para el envio de datos de las posiciones de los objetos móviles.
## Changed
- El programa principal se divide en dos, en cliente y en servidor.
- Paso de un vector de esferas a una sola esfera, simplificando el envio de datos al cliente.
- Paso de if else a switch para el bot.
- Variación del radio de manera circular(vuelve al estado inicial).
## Removed
- Tenis.cpp y la clase Mundo.


## [3.1] - 2021-11-21
### Added
- Tratamiento de la señal SIGINT para poder desmapear el fd en el bot.
- Fin de partida al llegar a 3 puntos cualquier jugador
### Changed
- Cerrado adecuado de los descriptores de fichero del mapeado de memoria.


## [3.0] - 2021-11-08
### Added
- Clase Logger, bot, Mensaje y DatosMemCompartida (funcionalidades en el guión de prácticas).
- Descriptor de FIFO en la clase Mundo.
- Proyección en memoria en la clase Mundo.
- Fin de juego al llegar a los 3 puntos algún jugador.


## [2.3] - 2021-10-28
### Changed
- Destrucción de esferas al alcanzar el mínimo de tamaño


## [2.2] - 2021-10-26
### Added
- Sobrecarga del operador sumador y restador de la clase Vector2D con datos tipo float
- Cambio del tamaño de las raquetas al ser impactadas por un proyectil
- Atributo radio de la Clase Disparo de valor constante
- Método de obtención del radio de Disparo
- Cambio de radio de la esfera.
### Changed
- Modificación del método Rebote de parámetro Disparo, inclución del radio del proyectil.
- De esfera simple a contenedor de esferas <vector>.
- Cambio de nombre de la instacia de Esfera de esfera esferas.
- Adaptación de las funciones en Mundo.cpp al nuevo contenedor de esferas.


## [2.1] - 2021-10-25
### Added
- Creación de la clase Disparo.
- Agregado de las instancias proyectiles de la clase Disparo.
- Agregado del método Rebote en la clase Plano para la detección de la colisión del Disparo con las paredes y las raquetas.

## [2.0] - 2021-10-25
### Added
- Nueva branch (práctica 2)
### Changed
- Redefinición del método mueve de la Esfera.
- Redefinición del método mueve de la Raqueta.


## [1.2] - 2021-10-8
### Changed
- Acomodar nombre del autor
- Eliminación de los apartados vacios del Changelog

## [1.1] - 2021-10-07
### Added
- Nombre del autor


## [1.0] - 2021-10-7
### Added
Se agregó el Changelog

