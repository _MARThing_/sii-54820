// Mundo.cpp: implementation of the CMundo class.
//// Author: "Martin Augusto Reigadas Teran";
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <time.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd_FIFO);
	fdS.Close();
	fdC.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	
	if(gameOver){
	char cad[100];
	
	if(puntos1>puntos2){
		sprintf(cad,"Ha ganado el jugador1: %d vs %d",puntos1,puntos2);
	}else if(puntos1<puntos2){
		sprintf(cad,"Ha ganado el jugador2: %d vs %d",puntos2,puntos1);
	}
	print(cad,10,0,1,1,1);
	
	}else{
	
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	if (proyectil1!=NULL)
		proyectil1->Dibuja();
	if (proyectil2!=NULL)
		proyectil2->Dibuja();
	}
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	// Creación de una variable local auxiliar para la reducción del tamaño de las raquetas
	
	float centro1,centro2;
	static time_t tiempo_past=time(NULL); //Timer
	Mensaje m;
	//Continuación del código entregado
	if(!gameOver){
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);	
	
	if ( proyectil1!=NULL )
		proyectil1->Mueve(0.025f);
	if( proyectil2!=NULL)
		proyectil2->Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		///////// Escritura en el FIFO  /////////////
		m.id=2;m.ptos=puntos2;
		write(fd_FIFO,&m,sizeof(m));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		/////// Escritura en el FIFO ////////
		m.id=1;m.ptos=puntos1;
		write(fd_FIFO,&m,sizeof(m));
	}
		
	// destrucción e interacción de los disparos
	if(proyectil1!=NULL){
		if(jugador2.Rebota(*proyectil1)){
			centro2=(jugador2.y2+jugador2.y1)/2;
			jugador2.y1=centro2+0.7*(jugador2.y1-centro2);
			jugador2.y2=centro2+0.7*(jugador2.y2-centro2);
			delete proyectil1;
			proyectil1=NULL;
		}else if(fondo_dcho.Rebota(*proyectil1)){
			delete proyectil1;
			proyectil1=NULL;
		}
	}
	
	if(proyectil2!=NULL){ // proyectil 2
		if(jugador1.Rebota(*proyectil2)){
			centro1=(jugador1.y1+jugador1.y2)/2;
			jugador1.y1=centro1+0.7*(jugador1.y1-centro1);
			jugador1.y2=centro1+0.7*(jugador1.y2-centro1);
			delete proyectil2;
			proyectil2=NULL;
		}else if(fondo_izq.Rebota(*proyectil2)){
			delete proyectil2;
			proyectil2=NULL;		
		}
	}
	
	//Variación del tamaño de las esferas
	if(difftime(time(NULL),tiempo_past)>20){
		if ((esfera.radio*=0.7f)<0.1f) esfera.radio=0.5f;
		tiempo_past=time(NULL);	
	}

	
	
	// Escritura en la tubería de los datos de las esferas y raquetas
	DatosCS aux;
	// Pasar datos a las variables
	aux.esfera = esfera ;
	aux.jugador1 = jugador1 ;
	aux.jugador2 = jugador2 ;
	aux.puntos1 = puntos1 ;
	aux.puntos2 = puntos2 ; 
	if(proyectil1!=NULL){
		aux.disparo1x = proyectil1->centro.x;
		aux.disparo1y = proyectil1->centro.y;
	}else{
		aux.disparo1x = 0xFFFF;
		aux.disparo1y = 0xFFFF;
	}
	
	if(proyectil2!=NULL){
		aux.disparo2x = proyectil2->centro.x;
		aux.disparo2y = proyectil2->centro.y;
	}else{
		aux.disparo2x = 0xFFFF;
		aux.disparo2y = 0xFFFF;
	}
	
	fdC.Send((void *) &aux,sizeof(aux));
	
	}
	
	if(puntos2==3 || puntos1==3){
		gameOver=true;
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	
}

void CMundo::RecibeComandosJugador(){
	 while (1) {
//	    printf("Esperando los datos del teclado\n");
            usleep(10);
            char dato [10];		
            fdC.Receive((void*) dato, sizeof(dato));
 //           printf("Recibi los datos del teclado\n");
            unsigned char key;
            int x,y;
            sscanf(dato,"%c %d %d",&key,&x,&y);
            
            float vel=5; // Variable de la velocidad de los proyectiles
	float centro1=(jugador1.y1+jugador1.y2)/2;
	float centro2=(jugador2.y2+jugador2.y1)/2;
	switch(key)
	{
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'e': 
		if(proyectil1==NULL)
			proyectil1 = new Disparo(jugador1.x1+1,centro1,vel); 
	break;
	case 'p':
		if(proyectil2==NULL) 
			proyectil2 = new Disparo(jugador2.x1-1,centro2,-vel);
	break;
	}
      }
}	

void* hilos_comandos(void* d){
	CMundo* p= (CMundo*) d;
	p->RecibeComandosJugador();
	return NULL;
}

void CMundo::Init()
{	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	// Enlace del socket de conexión a una IP y un puerto
	char nombre[]="192.168.88.201";
	int puerto =1030;
	fdS.InitServer(nombre,puerto);
	fdC=fdS.Accept();
	char name[12];
	fdC.Receive((void*)name,sizeof(name));
	
	// Apertura tubería loger
	if((fd_FIFO=open("FIFO",O_WRONLY))<0){
		std::cerr<<"Error al abrir la tuberia"<<std::endl;
	}
	
	
	pthread_create(&hilo, NULL, hilos_comandos, this);
	gameOver=false;

}
