#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <string.h>
#include "Mensaje.h"

/*
class Mensaje {
public:
	int id;
	int ptos;
};

friend ostream& operator<<(ostream& os,Mensaje m){
	os<<"Jugador "<<m.id<<" marca 1 punto, lleva un total de "<<m.ptos<<" puntos"<<std::endl;
	return os;
}*/


int main(int argc,char * argv[]){
	int fd,rd;
	// Elimina cualquier tubería existente
	unlink("FIFO");
	// crea el fifo
	if(mkfifo("FIFO",0777)<0){
		fprintf(stderr,"Imposible de crear el FIFO\n");
		return 1;
	}
		// Abre el fifo en solo lectura
	if((fd=open("FIFO",O_RDONLY))<0){
		fprintf(stderr,"Error al abrir el fifo\n");
		return 1;	
	}
	
	Mensaje m;
	while(read(fd,&m,sizeof(m))==sizeof(m)){
		printf("Jugador %d marca 1 punto, lleva un total de %d puntos\n",m.id,m.ptos);
	}
	close(fd);
	unlink("FIFO");
	return 0;
}
