// Mundo.cpp: implementation of the CMundo class.
//// Author: "Martin Augusto Reigadas Teran";
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <time.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	munmap(pmem,bstat.st_size);
	fdC.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	
	if(gameOver){
	char cad[100];
	
	if(puntos1>puntos2){
		sprintf(cad,"Ha ganado el jugador1: %d vs %d",puntos1,puntos2);
	}else if(puntos1<puntos2){
		sprintf(cad,"Ha ganado el jugador2: %d vs %d",puntos2,puntos1);
	}
	print(cad,10,0,1,1,1);
	
	}else{
	
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	if (proyectil1!=NULL)
		proyectil1->Dibuja();
	if (proyectil2!=NULL)
		proyectil2->Dibuja();
	}
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	// Creación de una variable local auxiliar para la reducción del tamaño de las raquetas
	
	// Llegada de datos por la tubería 
	DatosCS aux;
	fdC.Receive((void*)&aux,sizeof(aux));
	//printf("Datos recibidos p1 %f %f y p2 es %f %f\n",aux.disparo1x,aux.disparo1y,aux.disparo2x,aux.disparo2y);
	// Pasar datos a las variables
	esfera=aux.esfera;
	jugador1=aux.jugador1;
	jugador2=aux.jugador2;
	puntos1=aux.puntos1;
	puntos2=aux.puntos2; 
	delete proyectil1;
	proyectil1=NULL;
	if(aux.disparo1x!=0xFFFF)
		proyectil1= new Disparo(aux.disparo1x,aux.disparo1y,0);
	
	delete proyectil2;
	proyectil2=NULL;
	if(aux.disparo2x!=0xFFFF)
		proyectil2=new Disparo(aux.disparo2x,aux.disparo2y,0);
		
	// EL BOT //
	pmem->esfera=esfera;
	pmem->raqueta1=jugador1;
	pmem->raqueta2=jugador2;
	//printf("Accion1 del bot %d\n",pmem->accion1);
	switch(pmem->accion1){
	case 1:
	OnKeyboardDown('w', 0,0); break; // sube
	case -1:
	OnKeyboardDown('s', 0,0); break; //baja
	default:;
	}
	
	switch(pmem->accion2){
	case 1:
	OnKeyboardDown('o', 0,0); break; // sube
	case -1:
	OnKeyboardDown('l', 0,0); break; // baja
	default:;
	}


	if(puntos2==3 || puntos1==3){
		gameOver=true;
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char dato [10];
	sprintf(dato,"%c %d %d",key,x,y);
	//printf("%s\n",dato);
	fdC.Send((void*)dato,sizeof(dato));
}

void CMundo::Init()
{	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	

	// Socket 
	char nombre[]="192.168.88.201";
	int puerto =1030;
	fdC.Connect(nombre,puerto);	
	char name[12]="Conectado\n";
	fdC.Send((void*)name,sizeof(name));
	
	
	// Creación del fichero de memoria compartida //
	int fd_aux;
	if((fd_aux=open("DATA",O_RDWR|O_CREAT|O_TRUNC,0666))<0){
		fprintf(stderr,"Error al crear fichero de memoria compartida");
	}
	//ftruncate(fd_aux,sizeof(mem));
	write(fd_aux,&mem,sizeof(mem));
	// PROYECCIÓN en memoria //

	if (fstat(fd_aux, &bstat)<0) {
		perror("Error en fstat del archivo");
		close(fd_aux);
		exit(-1);
	}
	/* Se proyecta el archivo */
	void * aux;
	aux=mmap((void*) &mem, bstat.st_size, PROT_READ|PROT_WRITE,MAP_SHARED, fd_aux, 0);
	//pmem=(DatosMemCompartida *)aux;
	if (aux== MAP_FAILED) {
		perror("Error en la proyeccion del archivo");
		close(fd_aux);
	}
	pmem=(DatosMemCompartida *) aux;
	close(fd_aux);
	
	gameOver=false;

}
