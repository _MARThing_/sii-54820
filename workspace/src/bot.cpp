
#ifndef  _DATA_
#include "DatosMemCompartida.h"
#endif

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <signal.h>

#include <ctime>

bool value;

void recepcion(int s){
	value=false;
	printf("Apagando Bot\n");
}

int main(){

DatosMemCompartida * pmem=NULL;
value=true;

struct sigaction senal;

senal.sa_handler=recepcion;
senal.sa_flags=0;
sigemptyset(&senal.sa_mask);
sigaction(SIGINT,&senal,NULL);


int fd_aux;
std::time_t tiempo;
	if((fd_aux=open("DATA",O_RDWR))<0){
		perror("Error al abrir fichero de memoria compartida");
	}
	// PROYECCIÓN en memoria //
	struct stat bstat;

	if (fstat(fd_aux, &bstat)<0) {
		perror("Error en fstat del archivo");
		close(fd_aux);
	}

	/* Se proyecta el archivo */
	pmem=(DatosMemCompartida *)mmap(0, bstat.st_size, PROT_READ|PROT_WRITE,MAP_SHARED, fd_aux, 0);
	if (pmem == (void *)MAP_FAILED) {
		perror("Error en la proyeccion del archivo");
		close(fd_aux);
	}
	close(fd_aux);
	tiempo=std::time(NULL);
	while(value){
	float ey=pmem->esfera.centro.y;
	float ry1=pmem->raqueta1.y1;
	float ry2=pmem->raqueta1.y2;
	float rx=pmem->raqueta1.x1;
	float ex=pmem->esfera.centro.x;
	float radio=pmem->esfera.radio;
	float r2y1=pmem->raqueta2.y1;
	float r2y2=pmem->raqueta2.y2;
//	printf("centro %.3f y1 %.3f y2 %.3f\n",ex-rx,ry1,ry2);
	usleep(25000);
		//if((ex-rx)<5){
		if((ey-radio)>ry1){
			pmem->accion1=1;
		}else if((ey+radio)<ry2){
			pmem->accion1=-1;
		}else{ 
			pmem->accion1=0;
		}
		
		if(((std::time(NULL)-tiempo)>10000)&& pmem->raqueta2.velocidad.y==0){
		
			if((ey-radio)>r2y1){
			pmem->accion2=1;
			}else if((ey+radio)<r2y2){
			pmem->accion2=-1;
			}else{ 
			pmem->accion2=0;
			}
			tiempo=std::time(NULL);
						
		}else{
			pmem->accion2=0;
			
		}

		}
		//}else{
		//	continue;
		//}
	//usleep(25000);
	
	munmap(pmem,bstat.st_size);
return 0;
}
