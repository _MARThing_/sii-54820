#include "Disparo.h"
#include "glut.h"

const float r=0.2f;

Disparo::Disparo(){	
	velocidad.x=0.0;
	velocidad.y=0.0;
	centro.x=0;
	centro.y=0;
	radio=r;
}

Disparo::Disparo(float x,float y,float v):
	centro(x,y),velocidad(v,0),radio(r)
{
	
}

Disparo::Disparo(Vector2D pos, Vector2D vel){
	centro=pos;
	velocidad=vel;
	radio=r;
}

Disparo::~Disparo(){
}

void Disparo::Mueve(float t){
	centro=centro+velocidad*t;
}

void Disparo::Dibuja(){
	glColor3ub(255,0,255);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

float Disparo::GetRadio(){
	return radio;
}
