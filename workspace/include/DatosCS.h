#ifndef _DATOSCS_
#define _DATOSCS_
#include "Raqueta.h"
#include "Esfera.h"


class DatosCS{
	public:
	double  disparo1x,disparo1y;
	double  disparo2x,disparo2y;
	Raqueta jugador1;
	Raqueta jugador2;
	Esfera esfera;
	int puntos1;
	int puntos2;
};


#endif /*_DATOSCS_*/
