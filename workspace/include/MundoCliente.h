// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _MUNDO_CLIENTE_
#define _MUNDO_CLIENTE_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Disparo.h"

#include <iostream>
#include "Mensaje.h" //Información por el  FIFO
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "DatosCS.h"

#include "DatosMemCompartida.h"
#include <sys/mman.h>

#include "Socket.h"
class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	Disparo* proyectil1=NULL;
	Disparo* proyectil2=NULL;
	
	int puntos1;
	int puntos2;
		
	DatosMemCompartida mem;
	DatosMemCompartida * pmem;
	DatosMemCompartida * pmem2;
	struct stat bstat;
	
	bool gameOver;
	
	Socket fdC;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
