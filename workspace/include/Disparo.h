// Disparo.h: interface for the Disparo class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AFX_Disparo
#define AFX_Disparo

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Disparo
{
public:
	Disparo();
	Disparo(Vector2D pos, Vector2D vel);
	Disparo(float x,float y,float v);
	virtual ~Disparo();
    	Vector2D  centro;
    	Vector2D  velocidad;
   
    	void Mueve(float t);
    	void Dibuja();
	float GetRadio();
private:
	float radio;
};


#endif
