# Juego del Tenis
### Autor: Martin Reigadas


## Descripción
El juego se basa en dos jugadores que tienen que conseguir pasar la esfera al otro lado de su oponente, por lo que va acumulando puntos, también puede disparar a su oponente para reducir su tamaño. Mientras avanza el juego las esferas se duplican y van reduciendo su tamaño.

## Instrucciones

### Jugador 1:
1. Presione 'w' para subir.
2. Presione 's' para bajar.
3. Presione 'e' para disparar.

### Jugador 2:
1. Presione 'o' para subir.
2. Presione 'l' para bajar.
3. Presione 'p' para disparar.



